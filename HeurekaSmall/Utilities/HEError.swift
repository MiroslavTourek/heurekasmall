//
//  HEError.swift
//  HeurekaSmall
//
//  Created by Miroslav Tourek on 07/04/2021.
//

import Foundation

enum HEError: String, Error {
    case unableToComplete
    case invalidResponse
    case invalidData
}
