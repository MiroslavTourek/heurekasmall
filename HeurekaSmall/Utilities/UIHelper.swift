//
//  UIHelper.swift
//  HeurekaSmall
//
//  Created by Miroslav Tourek on 08/04/2021.
//

import UIKit

struct UIHelper {
    
    // MARK: - Navigation controller
    
    static func setupAppearance(for controller: UINavigationController) {
        controller.navigationBar.standardAppearance = UIHelper.setupNavigationBarAppearance()
        controller.navigationBar.tintColor = .systemBackground
        controller.navigationBar.backgroundColor = UIColor(named: HEColors.HEBlue.rawValue)
        controller.navigationBar.prefersLargeTitles = true
    }
    
    static func setupNavigationBarAppearance() -> UINavigationBarAppearance {
        let navigationBarAppearance = UINavigationBarAppearance()
        navigationBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.systemBackground]
        
        return navigationBarAppearance
    }
    
    // MARK: - Status bar
    
    static func changeStatusBar(backgroundColor: UIColor, at controller: UIViewController) {
        let scene = UIApplication.shared.connectedScenes.first as! UIWindowScene
        let window = scene.windows.first
        
        let statusBar = UIView(frame: (scene.statusBarManager?.statusBarFrame)!)
        statusBar.backgroundColor = backgroundColor
        window?.addSubview(statusBar)
        
        controller.setNeedsStatusBarAppearanceUpdate()
    }
    
    // MARK: - Collection view flow layouts
    
    static func createOneRowFourColumnsPagedVerticalFlowLayout(height: CGFloat, interItemSpacing: CGFloat) -> UICollectionViewLayout {
        
        let pageLayout = UICollectionViewCompositionalLayout { _,_ in
            let columns = 4
            let rowGroupHeight = height
            
            let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
                                                  heightDimension: .fractionalHeight(1))
            let item = NSCollectionLayoutItem(layoutSize: itemSize)
            
            let rowGroupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.9),
                                                      heightDimension: .absolute(rowGroupHeight))
            let rowGroup = NSCollectionLayoutGroup.horizontal(layoutSize: rowGroupSize,
                                                              subitem: item,
                                                              count: columns)
            rowGroup.interItemSpacing = NSCollectionLayoutSpacing.fixed(interItemSpacing)
            
            let pageLayoutSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
                                                                  heightDimension: .fractionalHeight(1))
            let pageGroup = NSCollectionLayoutGroup.vertical(layoutSize: pageLayoutSize,
                                                                  subitem: rowGroup,
                                                                  count: 1)
            
            let orthogonalScrollingBehavior = UICollectionLayoutSectionOrthogonalScrollingBehavior.paging
            let page = NSCollectionLayoutSection(group: pageGroup)
            page.orthogonalScrollingBehavior = orthogonalScrollingBehavior
            
            page.contentInsets = NSDirectionalEdgeInsets(top: 0,
                                                         leading: interItemSpacing,
                                                         bottom: 0,
                                                         trailing: interItemSpacing)
            
            return page
        }
        
        return pageLayout
    }
    
    static func createOffersListFlowLayout(interItemSpacing: CGFloat) -> UICollectionViewLayout {
        let columns = 1
        
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
                                              heightDimension: .fractionalHeight(0.8))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let rowGroupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
                                                  heightDimension: .estimated(140))//.fractionalHeight(1))
        let rowGroup = NSCollectionLayoutGroup.horizontal(layoutSize: rowGroupSize,
                                                          subitem: item,
                                                          count: columns)
        
        let section = NSCollectionLayoutSection(group: rowGroup)
        section.interGroupSpacing = interItemSpacing
        section.contentInsets = NSDirectionalEdgeInsets(top: 10,
                                                        leading: 10,
                                                        bottom: 10,
                                                        trailing: 10)
        
        return UICollectionViewCompositionalLayout(section: section)
    }
    
    static func createTableFlowLayout() -> UICollectionViewLayout {
        let config = UICollectionLayoutListConfiguration(appearance: .grouped)
        return UICollectionViewCompositionalLayout.list(using: config)
    }
    
}
