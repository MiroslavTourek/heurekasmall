//
//  Constants.swift
//  HeurekaSmall
//
//  Created by Miroslav Tourek on 08/04/2021.
//

enum ImageName: String {
    case all, garden, music, telephone, placeholder
}

enum HEColors: String {
    case HEBlue
}

enum CustomCategoryIds: Int {
    case all = -1
}
