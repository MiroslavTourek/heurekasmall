//
//  FilterCollectionCell.swift
//  HeurekaSmall
//
//  Created by Miroslav Tourek on 08/04/2021.
//

import UIKit

class FilterCollectionCell: UICollectionViewCell {
    
    // MARK: - Static
    
    static let reuseID = "FilterCollectionCell"
    
    // MARK: - Properties
    
    let selectedColor: UIColor = .systemRed
    
    override var isSelected: Bool {
        didSet {
            contentView.backgroundColor = isSelected ? selectedColor : .clear
        }
    }
    
    // MARK: - Private properties
    
    private var imageContainerView: HEImageContainerView!
    private let imageView = HEImageView(frame: .zero)
    private let nameLabel = HETitleLabel(textAlignment: .center, fontSize: 12, isBold: false)
    
    private let padding: CGFloat = 18
    private let imageLabelSpacing: CGFloat = 12
    private let imageContainerViewPadding: CGFloat = 3
    
    // MARK: - init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configure()
        layoutUI()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Public methods
    
    func setup(categoryId: Int, title: String?) {
        let image = getCategoryImage(forID: categoryId)
        imageView.image = image
        nameLabel.text = title ?? localizedString(for: .filter) + " \(categoryId)"
        nameLabel.textColor = .systemBackground
    }
    
    // MARK: - Private methods
    
    private func getCategoryImage(forID Id: Int) -> UIImage? {
        let imageName: String
        
        switch Id {
        case 1:
            imageName = ImageName.telephone.rawValue
        case 2:
            imageName = ImageName.garden.rawValue
        case 3:
            imageName = ImageName.music.rawValue
        case CustomCategoryIds.all.rawValue:
            imageName = ImageName.all.rawValue
        default:
            imageName = ImageName.placeholder.rawValue
        }
        
        return UIImage(named: imageName)
    }
    
    private func configure() {
        contentView.layer.cornerRadius = 10
        contentView.layer.masksToBounds = true
        
        imageContainerView = HEImageContainerView(cornerRadius: frame.width / 2 - imageContainerViewPadding)
        
        nameLabel.numberOfLines = 2
        nameLabel.lineBreakMode = .byWordWrapping
    }
    
    private func layoutUI() {
        let subviews = [ imageContainerView!, imageView, nameLabel ]
        subviews.forEach { addSubview($0) }
        
        let nameLabelHeight = CGFloat(nameLabel.numberOfLines) * nameLabel.font.lineHeight * 1.1
        
        NSLayoutConstraint.activate([
            imageContainerView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: imageContainerViewPadding),
            imageContainerView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: imageContainerViewPadding),
            imageContainerView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -imageContainerViewPadding),
            imageContainerView.heightAnchor.constraint(equalTo: imageContainerView.widthAnchor),
            
            imageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: padding),
            imageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: padding),
            imageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -padding),
            imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor),
            
            nameLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            nameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            nameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            nameLabel.heightAnchor.constraint(equalToConstant: nameLabelHeight)
        ])
    }
    
}
