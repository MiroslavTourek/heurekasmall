//
//  OfferCollectionCell.swift
//  HeurekaSmall
//
//  Created by Miroslav Tourek on 13/04/2021.
//

import UIKit

class OfferCollectionCell: UICollectionViewCell {
    
    // MARK: - Static
    
    static let reuseID = "OfferCollectionCell"
    
    // MARK: - Properties
    
    var defaultBackgroundColor: UIColor = .systemGray5
    var selectedColor: UIColor = UIColor(named: HEColors.HEBlue.rawValue)!
    var fontHeight: CGFloat = 20
    var interLabelSpacing: CGFloat = 5
    var padding: CGFloat!

    override var isSelected: Bool {
        didSet {
            backgroundColor = isSelected ? selectedColor : defaultBackgroundColor
        }
    }
    
    // MARK: - Private properties
    
    private var titleLabel: HETitleLabel!
    private var priceLabel: HETitleLabel!
    
    // MARK: - init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configure()
        layoutUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Public methods
    
    func setup(offerId: Int, title: String?, price: Float?) {
        let tempPrice = price ?? 0
        titleLabel.text = title ?? localizedString(for: .offer) + " \(offerId)"
        priceLabel.text = tempPrice == 0 ? "N/A" : String(format: "%.f %@", tempPrice, "Kč")

        titleLabel.textColor = .label
        priceLabel.textColor = .label
    }
    
    // MARK: - Private methods
    
    private func configure() {
        layer.cornerRadius = 10
        backgroundColor = defaultBackgroundColor
        
        padding = contentView.frame.width * 0.04
        
        titleLabel = HETitleLabel(textAlignment: .natural, fontSize: fontHeight, isBold: false)
        priceLabel = HETitleLabel(textAlignment: .natural, fontSize: fontHeight, isBold: false)
        
        titleLabel.numberOfLines = 0
        titleLabel.lineBreakMode = .byWordWrapping
        
        priceLabel.numberOfLines = 0
        priceLabel.lineBreakMode = .byTruncatingTail
    }
    
    private func layoutUI() {
        let subviews = [ titleLabel!, priceLabel! ]
        subviews.forEach { addSubview($0) }
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: padding),
            titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -padding),
            
            priceLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor),
            priceLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: padding),
            priceLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -padding),
            priceLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -15)
        ])
    }
    
}
