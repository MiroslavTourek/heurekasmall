//
//  HENavigationViewController.swift
//  HeurekaSmall
//
//  Created by Miroslav Tourek on 12/04/2021.
//

import UIKit

class HENavigationViewController: UINavigationController {
    
    // MARK: - Properties
    
    var statusBarStyle = UIStatusBarStyle.lightContent {
        didSet {
            setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        statusBarStyle
    }
    
    // MARK: - init
    
    init(rootViewController: UIViewController, statusBarStyle: UIStatusBarStyle = .lightContent) {
        self.statusBarStyle = statusBarStyle
        super.init(rootViewController: rootViewController)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        setupAppearance()
        updateStatusBarColor()
    }
    
    // MARK: - Methods
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)

        if traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) {
            updateStatusBarColor()
        }
    }
    
    // MARK: - Private methods
    
    private func setupAppearance() {
        UIHelper.setupAppearance(for: self)
    }
    
    private func updateStatusBarColor() {
        switch traitCollection.userInterfaceStyle {
        case .unspecified:
            statusBarStyle = .default
        case .light:
            statusBarStyle = .lightContent
        case .dark:
            statusBarStyle = .darkContent
        @unknown default:
            fatalError()
        }
    }

}

// MARK: - HEAlertVCDelegate

extension HENavigationViewController: HEAlertVCDelegate {
    
    func alertButtonDidTapped(error: HEError, sender: UINavigationController, realSender: UIViewController) {
        
        guard let currentLastVC = sender.viewControllers.last,
              currentLastVC == realSender,
              let realSender = realSender as? AbleToReceiveErrors else { return }
        
        realSender.alertButtonDidTapped(error: error)
    }
    
}
