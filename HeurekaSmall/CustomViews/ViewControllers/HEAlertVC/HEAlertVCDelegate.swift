//
//  HEAlertVCDelegate.swift
//  HeurekaSmall
//
//  Created by Miroslav Tourek on 09/04/2021.
//

import UIKit

protocol HEAlertVCDelegate: class {
    func alertButtonDidTapped(error: HEError, sender: UINavigationController, realSender: UIViewController)
}

// Default implementation
extension HEAlertVCDelegate where Self: UINavigationController {
    
    func presentHEAlertOnMainThread(title: String, message: String, buttonTitle: String, error: HEError? = nil, sender: UIViewController? = nil) {
        DispatchQueue.main.async {
            let alert = HEAlertVC(title: title, message: message, buttonTitle: buttonTitle, sender: self, realSender: sender, error: error)
            alert.delegate = self
            alert.modalPresentationStyle = .overFullScreen
            alert.modalTransitionStyle = .crossDissolve
            self.present(alert, animated: true)
        }
    }
}
