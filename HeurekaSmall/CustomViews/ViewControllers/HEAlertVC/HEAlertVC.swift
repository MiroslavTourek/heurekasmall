//
//  HEAlertVC.swift
//  HeurekaSmall
//
//  Created by Miroslav Tourek on 07/04/2021.
//

import UIKit

class HEAlertVC: UIViewController, Localizable {
    
    // MARK: - Properties
    
    weak var delegate: HEAlertVCDelegate?
    
    // MARK: - Private properties
    
    private let containerView = UIView()
    private let titleLabel = HETitleLabel(textAlignment: .center, fontSize: 20)
    private let messageLabel = HEBodyLabel(textAlignment: .center)
    private var button: HEButton!
    
    private var alertTitle: String?
    private var message: String?
    private var buttonTitle: String?
    private var sender: UINavigationController!
    private var realSender: UIViewController?
    private var error: HEError?
    
    private var padding: CGFloat = 20
    
    // MARK: - init
    
    init(title: String, message: String, buttonTitle: String, sender: UINavigationController, realSender: UIViewController?, error: HEError?) {
        super.init(nibName: nil, bundle: nil)
        
        self.alertTitle = title
        self.message = message
        self.buttonTitle = buttonTitle
        self.sender = sender
        self.realSender = realSender
        self.error = error
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        configureContainerView()
        configureTitleLabel()
        configureMessageLabel()
        configureButton()
        
        layoutUI()
    }
    
    // MARK: - Private methods
    
    private func configureView() {
        view.backgroundColor = UIColor.black.withAlphaComponent(0.75)
    }
    
    private func configureContainerView() {
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.backgroundColor = .systemBackground
        containerView.layer.cornerRadius = 16
        containerView.layer.borderWidth = 2
        containerView.layer.borderColor = UIColor.white.cgColor
    }
    
    private func configureTitleLabel() {
        titleLabel.text = alertTitle ?? (error == nil ?
                                            "" : localizedString(for: .somethingWentWrong))
    }
    
    private func configureMessageLabel() {
        messageLabel.text = message ?? (error == nil ?
                                            "" : localizedErrorDescription(for: .unableToComplete))
        messageLabel.numberOfLines = 0
    }
    
    private func configureButton() {
        let defaultButtonTitle = localizedString(for: .ok)
        let buttonColor = error == nil ? UIColor(named: HEColors.HEBlue.rawValue)! : .systemRed
        button = HEButton(backgroundColor: buttonColor,
                          title: buttonTitle ?? defaultButtonTitle)
        button.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
    }
    
    private func layoutUI() {
        let subviews = [ containerView, titleLabel, messageLabel, button! ]
        
        subviews.forEach { view.addSubview($0) }
        
        let multiplier: CGFloat = error == nil ? 0.9 : 0.75
        
        NSLayoutConstraint.activate([
            containerView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            containerView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            containerView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: multiplier),
            containerView.heightAnchor.constraint(lessThanOrEqualTo: view.safeAreaLayoutGuide.heightAnchor, multiplier: 0.8),
            
            titleLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: padding),
            titleLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor,
                                                constant: padding),
            titleLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor,
                                                 constant: -padding),
            titleLabel.heightAnchor.constraint(equalToConstant: 28),
            
            button.bottomAnchor.constraint(equalTo: containerView.bottomAnchor,
                                           constant: -padding),
            button.leadingAnchor.constraint(equalTo: containerView.leadingAnchor,
                                            constant: padding),
            button.trailingAnchor.constraint(equalTo: containerView.trailingAnchor,
                                            constant: -padding),
            button.heightAnchor.constraint(equalToConstant: 44),
            
            messageLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor,
                                              constant: padding),
            messageLabel.bottomAnchor.constraint(equalTo: button.topAnchor,
                                                 constant: -padding),
            messageLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor,
                                                  constant: padding),
            messageLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor,
                                                   constant: -padding)
        ])
    }
    
    // MARK: - @objc methods
    
    @objc func dismissVC() {
        dismiss(animated: true)
        
        // Only dismiss if this alert isn't an error alert.
        guard let error = error,
              let realSender = realSender else { return }
        
        delegate?.alertButtonDidTapped(error: error, sender: sender, realSender: realSender)
    }

}
