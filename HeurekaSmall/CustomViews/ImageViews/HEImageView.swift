//
//  HEImageView.swift
//  HeurekaSmall
//
//  Created by Miroslav Tourek on 08/04/2021.
//

import UIKit

class HEImageView: UIImageView {
    
    // MARK: - Private properties
    
    private let cache = NetworkManager.shared.cache
    
    // MARK: - init
    
    convenience init(image: UIImage, cornerRadius: CGFloat = 0) {
        self.init(frame: .zero)
        self.image = image
        self.layer.cornerRadius = cornerRadius
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Methods
    
    func downloadImage(from urlString: String, for product: Product) {
        
        let cacheKey = NSString(string: "\(product.productId)")
        if let cachedImage = cache.object(forKey: cacheKey) {
            self.image = cachedImage
            return
        }
        
        guard let url = URL(string: urlString) else { return }
        
        let task = URLSession.shared.dataTask(with: url) { [weak self] data, response, error in
            guard let self = self,
                  error == nil,
                  let response = response as? HTTPURLResponse,
                  response.statusCode == 200,
                  let data = data,
                  let image = UIImage(data: data) else {
                return
            }
            
            self.cache.setObject(image, forKey: cacheKey)
            
            DispatchQueue.main.async {
                self.image = image
            }
        }
        
        task.resume()
        
    }
    
    // MARK: - Private methods
    
    private func configure() {
        clipsToBounds = true
        contentMode = .scaleAspectFill
        translatesAutoresizingMaskIntoConstraints = false
    }
    
}
