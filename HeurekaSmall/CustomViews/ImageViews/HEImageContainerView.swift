//
//  HEImageContainerView.swift
//  HeurekaSmall
//
//  Created by Miroslav Tourek on 11/04/2021.
//

import UIKit

class HEImageContainerView: UIView {

    // MARK: - init
    
    convenience init(backgroundColor: UIColor? = .systemBackground, cornerRadius: CGFloat = 0) {
        self.init(frame: .zero)
        self.backgroundColor = backgroundColor
        self.layer.cornerRadius = cornerRadius
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private methods
    
    private func configure() {
        translatesAutoresizingMaskIntoConstraints = false
    }

}
