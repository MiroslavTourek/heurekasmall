//
//  HEBodyLabel.swift
//  HeurekaSmall
//
//  Created by Miroslav Tourek on 07/04/2021.
//

import UIKit

class HEBodyLabel: UILabel {

    // MARK: - init
    
    convenience init(textAlignment: NSTextAlignment) {
        self.init(frame: .zero)
        self.textAlignment = textAlignment
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Ptivate methods
    
    private func configure() {
        textColor = .secondaryLabel
        font = .preferredFont(forTextStyle: .body) // dynamic type
        adjustsFontSizeToFitWidth = true
        minimumScaleFactor = 0.75
        lineBreakMode = .byWordWrapping
        translatesAutoresizingMaskIntoConstraints = false
    }

}
