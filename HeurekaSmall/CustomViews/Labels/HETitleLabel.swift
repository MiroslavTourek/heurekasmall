//
//  HETitleLabel.swift
//  HeurekaSmall
//
//  Created by Miroslav Tourek on 07/04/2021.
//

import UIKit

class HETitleLabel: UILabel {

    // MARK: - init
    
    convenience init(textAlignment: NSTextAlignment, fontSize: CGFloat, isBold: Bool = true) {
        self.init(frame: .zero)
        self.textAlignment = textAlignment
        self.font = isBold ? .boldSystemFont(ofSize: fontSize) : .systemFont(ofSize: fontSize)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Ptivate methods
    
    private func configure() {
        textColor = .label
        adjustsFontSizeToFitWidth = true
        minimumScaleFactor = 0.9
        lineBreakMode = .byTruncatingTail
        translatesAutoresizingMaskIntoConstraints = false
    }
    
}
