//
//  UINavigationController+.swift
//  HeurekaSmall
//
//  Created by Miroslav Tourek on 12/04/2021.
//

import UIKit

extension UINavigationController {
    
    override open var childForStatusBarStyle: UIViewController? {
        return self.topViewController
    }
    
}
