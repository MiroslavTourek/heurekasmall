//
//  UIView+.swift
//  HeurekaSmall
//
//  Created by Miroslav Tourek on 08/04/2021.
//

import UIKit

extension UIView {
    
    func localizedString(for key: LocalizationKey) -> String {
        NSLocalizedString(key.rawValue, comment: "")
    }
    
}
