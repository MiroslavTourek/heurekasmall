//
//  UIViewController+.swift
//  HeurekaSmall
//
//  Created by Miroslav Tourek on 07/04/2021.
//

import UIKit
import SafariServices

fileprivate var containerView: UIView!
fileprivate var isLoadingViewActive = false

extension UIViewController {
    
    func presentSafariVC(with stringUrl: String?) {
        
        guard let stringUrl = stringUrl,
              let url = URL(string: stringUrl) else {
            
            return }
        
        let safariVC = SFSafariViewController(url: url)
        safariVC.preferredControlTintColor = .systemBackground
        safariVC.preferredBarTintColor = UIColor(named: HEColors.HEBlue.rawValue)
        
        present(safariVC, animated: true)
    }
    
    func showLoadingView() {
        
        DispatchQueue.main.async {
            guard !isLoadingViewActive else { return }
            
            isLoadingViewActive = true
            
            containerView = UIView(frame: self.view.bounds) // full screen
            self.view.addSubview(containerView)
            
            containerView.backgroundColor = .systemBackground
            containerView.alpha = 0
            
            UIView.animate(withDuration: 0.25) {
                containerView.alpha = 0.5
            }
            
            let activityIndicator = UIActivityIndicatorView(style: .large)
            containerView.addSubview(activityIndicator)
            
            activityIndicator.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                activityIndicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
                activityIndicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor)
            ])
            
            activityIndicator.startAnimating()
        }
    }
    
    func dismissLoadingView() {
        
        isLoadingViewActive = false
        
        DispatchQueue.main.async {
            guard containerView != nil else { return }
            containerView.removeFromSuperview()
            containerView = nil
        }
    }
    
    func showEmptyStateView(with message: String, in view: UIView) {
        
        // TODO: implement
        
    }
    
}
