//
//  LocalizationKey.swift
//  HeurekaSmall
//
//  Created by Miroslav Tourek on 07/04/2021.
//

enum LocalizationKey: String {
    case cancel
    case ok
    case somethingWentWrong
    case filter
    case products
    case all
    case group
    case product
    case offer
    case whatDoYouWantToDo
    case seeOfferDetail
    case viewWebsite
}
