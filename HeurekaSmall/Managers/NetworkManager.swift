//
//  NetworkManager.swift
//  HeurekaSmall
//
//  Created by Miroslav Tourek on 07/04/2021.
//

import UIKit

class NetworkManager {
    
    // MARK: - Properties
    
    let cache = NSCache<NSString, UIImage>()
    
    // MARK: - Private properties
    
    private let baseUrl = "https://heureka-testday.herokuapp.com"
    
    // MARK: - Singleton
    
    static let shared = NetworkManager()
    
    private init() {}
    
    // MARK: - Methods
    
    func validate(stringUrl: String) -> Bool {
        guard let url = URL(string: stringUrl),
              UIApplication.shared.canOpenURL(url) else { return false }
        
        return true
    }
    
    func getCategories(completion: @escaping (Result<[Category], HEError>) -> Void) {
        
        let endpoint = baseUrl + "/categories/"
        
        fetch(url: endpoint) { (result: Result<[Category], HEError>) in
            switch result {
            case.success(let categories):
                completion(.success(categories))
                
            case.failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func getProducts(categoryId: Int, offset: Int = 0, limit: Int = 100, completion: @escaping (Result<[Int: [Product]], HEError>) -> Void) {
        
        let endpoint = baseUrl + "/products/\(categoryId)/\(offset)/\(limit)"
        
        var data = [Int: [Product]]()
        
        fetch(url: endpoint) { (result: Result<[Product], HEError>) in
            switch result {
            case.success(let products):
                data[categoryId] = products
                completion(.success(data))
                
            case.failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func getProducts(categoryIds: [Int], offsets: [Int], limit: Int, completion: @escaping (Result<[Int: [Product]], HEError>) -> Void) {
        
        let idsCount = categoryIds.count
        var receivedResults = 0
        
        var data = [Int: [Product]]()
        
        for i in 0..<idsCount {
            let currentCategoryId = categoryIds[i]
            let currentCategoryOffset = offsets[i]
            let endpoint = baseUrl + "/products/\(currentCategoryId)/\(currentCategoryOffset)/\(limit)"
            
            fetch(url: endpoint) { (result: Result<[Product], HEError>) in
                receivedResults += 1
                
                switch result {
                case.success(let products):
                    data[currentCategoryId] = products
                    
                    if receivedResults == idsCount {
                        completion(.success(data))
                        return
                    }
                    
                case.failure(let error):
                    guard !data.isEmpty else { return completion(.failure(error)) }
                    completion(.success(data))
                    return
                }
            }
        }
    }
    
    func getOffers(productId: Int, offset: Int = 0, limit: Int = 100, completion: @escaping (Result<[Offer], HEError>) -> Void) {
        
        let endpoint = baseUrl + "/offers/\(productId)/\(offset)/\(limit)"
        
        fetch(url: endpoint) { (result: Result<[Offer], HEError>) in
            switch result {
            case.success(let offers):
                completion(.success(offers))
                
            case.failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func getCountOfProductFor(categoryId: Int, completion: @escaping (Result<Counter, HEError>) -> Void) {
        
        let endpoint = baseUrl + "/products/\(categoryId)/count/"
        
        fetchCount(url: endpoint) { (result: Result<Counter, HEError>) in
            switch result {
            case.success(let counter):
                completion(.success(counter))
                
            case.failure(let error):
                completion(.failure(error))
            }
        }
        
    }
    
    func getCountOfProductFor(categoryIds: [Int], completion: @escaping (Result<Counter, HEError>) -> Void) {
        
        let idsCount = categoryIds.count
        var receivedResults = 0
        
        var resultCounter = Counter(count: 0)
        var count = 0 {
            didSet {
                resultCounter.count = resultCounter.count! + count
            }
        }
        
        for i in 0..<idsCount {
            let currentCategoryId = categoryIds[i]
            let endpoint = baseUrl + "/products/\(currentCategoryId)/count/"
            
            fetchCount(url: endpoint) { (result: Result<Counter, HEError>) in
                receivedResults += 1
                
                switch result {
                case.success(let counter):
                    count = counter.count ?? 0
                    
                    if receivedResults == idsCount {
                        completion(.success(resultCounter))
                        return
                    }
                    
                case.failure(let error):
                    guard let count = resultCounter.count,
                          count > 0 else { return completion(.failure(error)) }
                    completion(.success(resultCounter))
                    return
                }
            }
        }
    }
    
    func getCountOfOffersFor(productId: Int, completion: @escaping (Result<Counter, HEError>) -> Void) {
        
        let endpoint = baseUrl + "/products/\(productId)/count/"
        
        fetchCount(url: endpoint) { (result: Result<Counter, HEError>) in
            switch result {
            case.success(let counter):
                completion(.success(counter))
                
            case.failure(let error):
                completion(.failure(error))
            }
        }
        
    }
    
    // MARK: - Private methods
    
    private func fetch<T: Codable>(url: String, completion: @escaping (Result<[T], HEError>) -> Void) {
        
        guard let url = URL(string: url) else {
            return completion(.failure(.unableToComplete))
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            
            if let _ = error {
                completion(.failure(.unableToComplete))
                return
            }
            
            guard let response = response as? HTTPURLResponse,
                  response.statusCode == 200 else {
                return completion(.failure(.invalidResponse))
            }
            
            guard let data = data else {
                return completion(.failure(.invalidData))
            }
            
            do {
                let decoder = JSONDecoder()
                let response = try decoder.decode([T].self, from: data)
                completion(.success(response))
            } catch {
                completion(.failure(.invalidData))
            }
        }
        
        task.resume()
    }
    
    private func fetchCount<T: Codable>(url: String, completion: @escaping (Result<T, HEError>) -> Void) {
        
        guard let url = URL(string: url) else {
            return completion(.failure(.unableToComplete))
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            
            if let _ = error {
                completion(.failure(.unableToComplete))
                return
            }
            
            guard let response = response as? HTTPURLResponse,
                  response.statusCode == 200 else {
                return completion(.failure(.invalidResponse))
            }
            
            guard let data = data else {
                return completion(.failure(.invalidData))
            }
            
            do {
                let decoder = JSONDecoder()
                let response = try decoder.decode(T.self, from: data)
                completion(.success(response))
            } catch {
                completion(.failure(.invalidData))
            }
        }
        
        task.resume()
    }
    
}
