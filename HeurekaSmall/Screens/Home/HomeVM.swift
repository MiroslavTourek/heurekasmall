//
//  HomeVM.swift
//  HeurekaSmall
//
//  Created by Miroslav Tourek on 09/04/2021.
//

import UIKit

// MARK: - Delegate protocol

protocol HomeVMDelegate: class {
    func scrollProductsCollectionViewTo(position: IndexPath)
    func selectAllCategoryAnd(fetchData: Bool)
    func presentAlert(with error: HEError)
    func startActivityView()
    func stopActivityView()
    func selectAllFilter()
}

// MARK: - Enums

enum Section: String {
    case main
}

// MARK: - Class

class HomeVM: Localizable {
    
    // MARK: - Properties
    
    weak var delegate: HomeVMDelegate?
    
    let titleLabelFontSize: CGFloat = 35
    var filterCollectionViewHeight: CGFloat!
    
    /// The categories are in the order received from the API. The non-API all category is always first.
    var categories: [Category] = []
    
    var categoriesDataSource: UICollectionViewDiffableDataSource<Section, Category>!
    var productsDataSource: UICollectionViewDiffableDataSource<String, Product>!
    
    var isSpecificCategorySelected = false
    var isPaging = false
    
    /// All selected categories except the all category
    var selectedSpecificCategories: [IndexPath] = []
    
    /// Contains all sections (All category selected) or all selected sections. It can't be empty. It is sorted by section Id.
    var selectedSections: [ProductSectionItem] = []
    
    let limit = 100
    
    // MARK: - Private properties
    
    private var getProductsCompletion: ((Result<[Int: [Product]], HEError>) -> Void)!
    
    private let defaultOffset = 0
    
    /// Offsets for currenty selected categories with category ids as keys.
    private var offsets: [Int: Int] = [:]
    
    // MARK: - Methods
    
    func configureCategoriesDataSource(for collectionView: UICollectionView) {
        categoriesDataSource = UICollectionViewDiffableDataSource<Section, Category>(collectionView: collectionView, cellProvider: { [weak self] collectionView, indexPath, category in
            
            guard let self = self,
                  let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FilterCollectionCell.reuseID, for: indexPath) as? FilterCollectionCell else {
                return UICollectionViewCell()
            }
            
            let category = self.categories[indexPath.row]
            cell.setup(categoryId: category.categoryId, title: category.title)
            
            return cell
        })
    }
    
    func configureProductsDataSource(for collectionView: UICollectionView) {
        let cellRegistration = UICollectionView.CellRegistration<UICollectionViewListCell, Product> { (cell, indexPath, product) in
            // Note: Opportunity to use the default cell, so I use it. In other circumstances, I would create a custom class. However, I am not a fan of cell inheritance, because in my experience cell inheritance leads to confusing code. We introduce dependencies into code, which is often rather customized. I know that the filter collection and product collection cells are similar and could used inheritance.
            let maxImageHeight: CGFloat = 44
            var content = cell.defaultContentConfiguration()
            content.text = product.title
            content.textProperties.color = .label
            content.image = UIImage(named: ImageName.placeholder.rawValue)
            content.imageProperties.cornerRadius = maxImageHeight / 2
            content.imageProperties.maximumSize = CGSize(width: maxImageHeight,
                                                         height: maxImageHeight)
            cell.contentConfiguration = content
        }
        
        productsDataSource = UICollectionViewDiffableDataSource<String, Product>(collectionView: collectionView, cellProvider: { collectionView, indexPath, product in
            collectionView.dequeueConfiguredReusableCell(using: cellRegistration,
                                                         for: indexPath,
                                                         item: product)
        })
    }
    
    func getFilterCollectionViewLayout(for view: UIView) -> UICollectionViewLayout {
        calculateFilterCollectionViewHeight(for: view)
        return UIHelper.createOneRowFourColumnsPagedVerticalFlowLayout(height: filterCollectionViewHeight, interItemSpacing: 15)
    }
    
    func getTableCollectionFlowLayout() -> UICollectionViewLayout {
        UIHelper.createTableFlowLayout()
    }
    
    func createSectionItems(from groupedProducts: [Int : [Product]]) -> [ProductSectionItem] {
        // Create Section items
        var sectionItems = groupedProducts.map {
            ProductSectionItem(sectionId: $0.key, title: nil, products: $0.value)
        }
        
        // Add name to each of them
        for (i, item) in sectionItems.enumerated() {
            let id = item.sectionId
            if let category = categories.first(where: { $0.categoryId == id }) {
                sectionItems[i].title = category.title ?? "Section \(category.categoryId)"
            }
        }
        return sectionItems
    }
    
    func updateSectionItems(with groupedProducts: [Int : [Product]]) -> [ProductSectionItem] {
        
        var tempSelectedSections = self.selectedSections
        
        for (i, section) in tempSelectedSections.enumerated() {
            var mutableSection = tempSelectedSections[i]
            guard let newProducts = groupedProducts[section.sectionId] else { continue }
            mutableSection.products += newProducts
            tempSelectedSections[i] = mutableSection
        }
        
        return tempSelectedSections
    }
    
    func updateSelectedSpecificSections(with indexPathToRemove: IndexPath) {
        guard let indexToRemove = selectedSections.firstIndex(where: {
            $0.sectionId == categories[indexPathToRemove.row].categoryId
        }) else { return }
        
        selectedSections.remove(at: indexToRemove)
        
        offsets = [:]
        
        if selectedSections.isEmpty {
            delegate?.selectAllCategoryAnd(fetchData: true)
            return
        }
        
        updateProducts()
    }
    
    func addCategoryToSelected(indexPath: IndexPath) {
        guard !selectedSpecificCategories.contains(indexPath) else { return }
        selectedSpecificCategories.append(indexPath)
    }
    
    func removeCategoryFromSelected(indexPath: IndexPath) {
        guard let index = selectedSpecificCategories.firstIndex(of: indexPath) else { return }
        selectedSpecificCategories.remove(at: index)
    }
    
    // MARK: - Networking
    
    func setGetProductsCallback() {
        getProductsCompletion = { [weak self] result in
            
            guard let self = self else { return }
            
            self.delegate?.stopActivityView()
            
            switch result {
            
            case .success(let groupedProducts):
                
                let needToRemoveOldData = !self.isSpecificCategorySelected || self.selectedSpecificCategories.count == 1
                
                if needToRemoveOldData && !self.isPaging {
                    self.selectedSections = []
                }
                
                var sectionItems: [ProductSectionItem]
                
                if self.isPaging {
                    sectionItems = self.updateSectionItems(with: groupedProducts)
                    self.selectedSections = []
                } else {
                    sectionItems = self.createSectionItems(from: groupedProducts)
                }
                 
                self.selectedSections.append(contentsOf: sectionItems)
                self.selectedSections.sort(by: { $0.sectionId < $1.sectionId })
                
                self.updateProducts()
                
            case .failure(let error):
                self.delegate?.presentAlert(with: error)
            }
        }
    }
    
    func getCategories() {
        
        delegate?.startActivityView()
        
        NetworkManager.shared.getCategories { [weak self] result in
            
            guard let self = self else { return }
            
            switch result {
            case .success(let categories):
                let allCategory = Category(categoryId: CustomCategoryIds.all.rawValue,
                                           title: self.localizedString(for: .all))
                self.categories = [allCategory] + categories
                self.updateDataSource(self.categoriesDataSource,
                                      sections: [Section.main],
                                      items: [self.categories]) {
                    self.delegate?.selectAllFilter()
                }
                
            case .failure(let error):
                self.delegate?.presentAlert(with: error)
            }
        }
    }
    
    func downloadNextProducts() {
        
        getAllProductCountForSelectedSections { [weak self] allProductsCount in
            
            guard let self = self else { return }
            
            let existsNextProducts = allProductsCount > self.calculateSelectedSectionItemsCount()
            
            guard existsNextProducts else { return }
            
            self.isPaging = true
            self.addToOffsets(value: self.limit)
            
            let selectedCategoryIds = self.getSelectedCategoryIds()
            let categoryOffsets = self.getCategoryOffsets(for: selectedCategoryIds)
            
            
            self.delegate?.startActivityView()
            
            NetworkManager.shared.getProducts(categoryIds: selectedCategoryIds,
                                              offsets: categoryOffsets,
                                              limit: self.limit,
                                              completion: self.getProductsCompletion)
        }
        
    }
    
    func addToOffsets(value: Int) {
        let selectedCategoryIds = getSelectedCategoryIds()
        var tempOffsets = offsets
        
        selectedCategoryIds.forEach {
            if let offset = offsets[$0] {
                tempOffsets[$0] = offset + value
            } else {
                tempOffsets[$0] = value
            }
        }
        
        offsets = tempOffsets
    }
    
    func getProductsForAllCategories() {
        var categoryIds = categories.map { $0.categoryId }
        categoryIds.remove(at: 0) // remove the all category Id
        offsets = [:]
        let categoryOffsets = getCategoryOffsets(for: categoryIds)
        
        NetworkManager.shared.getProducts(categoryIds: categoryIds,
                                          offsets: categoryOffsets,
                                          limit: limit,
                                          completion: getProductsCompletion)
    }
    
    func getProductsFor(categoryId: Int) {
        NetworkManager.shared.getProducts(categoryId: categoryId,
                                          offset: defaultOffset,
                                          limit: limit,
                                          completion: getProductsCompletion)
    }
    
    // MARK: - Private methods
    
    private func updateProducts() {
        var selectedSectionNames = [String]()
        var selectedSectionItems = [[Product]]()
        
        selectedSections.forEach {
            selectedSectionNames.append($0.title ?? "Section \($0.sectionId)")
            selectedSectionItems.append($0.products)
        }
        
        updateDataSource(productsDataSource,
                         sections: selectedSectionNames,
                         items: selectedSectionItems) { [weak self] in
            let positionToScroll = IndexPath(item: 0, section: 0)
            self?.delegate?.scrollProductsCollectionViewTo(position: positionToScroll)
        }
    }
    
    private func updateDataSource<S: Hashable, I: Hashable>(_ dataSource: UICollectionViewDiffableDataSource<S, I>, sections: [S], items: [[I]], completion: (() -> Void)? = nil) {
        var snapshot = NSDiffableDataSourceSnapshot<S, I>()
        snapshot.appendSections(sections)
        
        for (i, section) in sections.enumerated() {
            snapshot.appendItems(items[i], toSection: section)
        }
        
        DispatchQueue.main.async {
            dataSource.apply(snapshot, animatingDifferences: true) {
                guard let completion = completion else { return }
                completion()
            }
        }
    }
    
    private func getAllProductCountForSelectedSections(completion: @escaping (Int) -> Void) {
        
        let isAllCategorySelected = !isSpecificCategorySelected
        
        var categoryIds = isAllCategorySelected ? categories.map { $0.categoryId } : getSelectedCategoryIds()
        
        categoryIds = categoryIds.filter { $0 > 0 } // remove the all category Id
        
        self.delegate?.startActivityView()

        NetworkManager.shared.getCountOfProductFor(categoryIds: categoryIds) { [weak self] result in
            
            guard let self = self else { return }
            
            self.delegate?.stopActivityView()

            switch result {
            case .success(let counter):
                guard let count = counter.count else {
                    completion(0)
                    return
                }
                completion(count)

            case .failure(_):
                completion(0)
            }

        }
    }
    
    /// This method returns the sorted currently selected category ids.
    /// - Returns: sorted currently selected category ids
    private func getSelectedCategoryIds() -> [Int] {
        var result = [Int]()
        selectedSections.forEach { result.append($0.sectionId) }
        
        return result
    }
    
    private func getCategoryIds(from selectedSections: [ProductSectionItem]) -> [Int] {
        selectedSections.map { $0.sectionId }
    }
    
    private func calculateSelectedSectionItemsCount() -> Int {
        var result = 0
        selectedSections.forEach { result += $0.products.count }
        
        return result
    }
    
    private func removeCategoryFromOffsets(categoryId: Int) {
        offsets[categoryId] = nil
    }
    
    private func getCategoryOffsets(for categoryIds: [Int]) -> [Int] {
        var categoryOffsets = [Int]()
        categoryIds.forEach { categoryOffsets.append(offsets[$0] ?? defaultOffset) }
        return categoryOffsets
    }
    
    private func calculateFilterCollectionViewHeight(for view: UIView) {
        filterCollectionViewHeight  = [view.frame.height * 1/6, 120].min() ?? 120
    }
}
