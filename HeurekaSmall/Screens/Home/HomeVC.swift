//
//  HomeVC.swift
//  HeurekaSmall
//
//  Created by Miroslav Tourek on 07/04/2021.
//

import UIKit

class HomeVC: UIViewController, ViewModelable, Localizable {
    
    // MARK: - Properties
    
    var viewModel: HomeVM!
    
    // MARK: - Private properties
    
    private let filterContainerView = UIView()
    private var filterTitleLabel: HETitleLabel!
    private var filterCollectionView: UICollectionView!
    private var productsTitleLabel: HETitleLabel!
    private var productsCollectionView: UICollectionView!
    
    // MARK: - Status bar
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return traitCollection.userInterfaceStyle == .light ? .lightContent : .darkContent
    }
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.setGetProductsCallback()

        configureViewController()
        configureFilterContainerView()
        configureFilterTitleLabel()
        configureFilterCollectionView()
        configureProductsTitleLabel()
        configureProductsCollectionView()
        
        layoutUI()
        
        configureDataSources()
        viewModel.getCategories()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    // MARK: - Private methods
    
    private func configureViewController() {
        view.backgroundColor = .systemBackground
        
        // Only for the purpose of the Back button.
        title = localizedString(for: .filter)
    }
    
    private func configureFilterContainerView() {
        filterContainerView.translatesAutoresizingMaskIntoConstraints = false
        filterContainerView.backgroundColor = UIColor(named: HEColors.HEBlue.rawValue)
    }
    
    private func configureFilterTitleLabel() {
        // This is our title.
        filterTitleLabel = HETitleLabel(textAlignment: .natural, fontSize: viewModel.titleLabelFontSize)
        filterTitleLabel.text = localizedString(for: .filter)
        filterTitleLabel.textColor = .systemBackground
    }
    
    private func configureFilterCollectionView() {
        let collectionViewLayout = viewModel.getFilterCollectionViewLayout(for: view)
        filterCollectionView = UICollectionView(frame: .zero,
                                                collectionViewLayout: collectionViewLayout)
        filterCollectionView.delegate = self
        filterCollectionView.backgroundColor = UIColor(named: HEColors.HEBlue.rawValue)
        filterCollectionView.isScrollEnabled = false
        filterCollectionView.translatesAutoresizingMaskIntoConstraints = false
        filterCollectionView.showsHorizontalScrollIndicator = false
        filterCollectionView.allowsMultipleSelection = true
        // Here I use the old-fashioned way of registering cells (iOS 13 and older). In other cases, I use cell registrators.
        filterCollectionView.register(FilterCollectionCell.self,
                                      forCellWithReuseIdentifier: FilterCollectionCell.reuseID)
    }
    
    private func configureProductsTitleLabel() {
        productsTitleLabel = HETitleLabel(textAlignment: .natural, fontSize: viewModel.titleLabelFontSize)
        productsTitleLabel.text = localizedString(for: .products)
    }
    
    private func configureProductsCollectionView() {
        let collectionViewLayout = viewModel.getTableCollectionFlowLayout()
        productsCollectionView = UICollectionView(frame: .zero,
                                                  collectionViewLayout: collectionViewLayout)
        productsCollectionView.delegate = self
        productsCollectionView.backgroundColor = .systemBackground
        productsCollectionView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    private func configureDataSources() {
        viewModel.configureCategoriesDataSource(for: filterCollectionView)
        viewModel.configureProductsDataSource(for: productsCollectionView)
    }
    
    private func selectAllCategory(fetchData: Bool = true) {
        let allFilterPosition = IndexPath(item: 0, section: 0)
        filterCollectionView.selectItem(at: allFilterPosition,
                                        animated: false,
                                        scrollPosition: .top)
        if fetchData {
            filterItemDidSelectedAt(indexPath: IndexPath(item: 0, section: 0))
        }
    }
    
    private func layoutUI() {
        
        let labelPadding: CGFloat = view.frame.width * 0.06
        
        let subviews = [
            filterContainerView,
            filterTitleLabel!,
            filterCollectionView!,
            productsTitleLabel!,
            productsCollectionView!
        ]
        
        subviews.forEach { view.addSubview($0) }
        
        NSLayoutConstraint.activate([
            filterContainerView.topAnchor.constraint(equalTo: view.topAnchor),
            filterContainerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            filterContainerView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            filterContainerView.bottomAnchor.constraint(equalTo: filterCollectionView.bottomAnchor, constant: 15),
            
            filterTitleLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 10),
            filterTitleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: labelPadding),
            filterTitleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -labelPadding),
            filterTitleLabel.heightAnchor.constraint(equalToConstant: viewModel.titleLabelFontSize + 5),
            
            filterCollectionView.topAnchor.constraint(equalTo: filterTitleLabel.bottomAnchor, constant: 10),
            filterCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            filterCollectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            filterCollectionView.heightAnchor.constraint(equalToConstant: viewModel.filterCollectionViewHeight),
            
            productsTitleLabel.topAnchor.constraint(equalTo: filterCollectionView.bottomAnchor, constant: 30),
            productsTitleLabel.leadingAnchor.constraint(equalTo: filterTitleLabel.leadingAnchor),
            productsTitleLabel.trailingAnchor.constraint(equalTo: filterTitleLabel.trailingAnchor),
            productsTitleLabel.heightAnchor.constraint(equalTo: filterTitleLabel.heightAnchor),
            
            productsCollectionView.topAnchor.constraint(equalTo: productsTitleLabel.bottomAnchor, constant: 20),
            productsCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            productsCollectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            productsCollectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
}

// MARK: - UICollectionViewDelegate

extension HomeVC: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        viewModel.isPaging = false
        
        switch collectionView {
        case filterCollectionView:
            filterItemDidSelectedAt(indexPath: indexPath)
            
        case productsCollectionView:
            productItemDidSelectedAt(indexPath: indexPath)
            
        default:
            print("Unknown collection view is used.")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        viewModel.isPaging = false
        
        switch collectionView {
        case filterCollectionView:
            
            let isAllCategoryDeselecting = !viewModel.isSpecificCategorySelected
            
            // All category cannot be deselected if it is active.
            if isAllCategoryDeselecting {
                selectAllCategory(fetchData: false)
                return
            }
            
            viewModel.removeCategoryFromSelected(indexPath: indexPath)
            viewModel.updateSelectedSpecificSections(with: indexPath)
            
        case productsCollectionView:
            break
            
        default:
            print("Unknown collection view is used.")
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        let onScreenHeight = scrollView.frame.size.height
        
        // Download next page
        if offsetY > contentHeight - onScreenHeight {
            
            viewModel.downloadNextProducts()
        }
    }
    
    // MARK: - Filter Collection View methods
    
    private func filterItemDidSelectedAt(indexPath: IndexPath) {
        
        showLoadingView()
        
        let categoryId = viewModel.categories[indexPath.row].categoryId
        let isAllCategorySelected = categoryId == CustomCategoryIds.all.rawValue
        
        guard !isAllCategorySelected else {
            viewModel.isSpecificCategorySelected = false
            deselectAllSpecificCategories()
            viewModel.getProductsForAllCategories()
            return
        }
        
        viewModel.isSpecificCategorySelected = true
        deselectAllCategory()
        viewModel.addCategoryToSelected(indexPath: indexPath)
        
        viewModel.getProductsFor(categoryId: categoryId)
        viewModel.addToOffsets(value: viewModel.limit)
    }
    
    private func deselectAllCategory() {
        filterCollectionView.deselectItem(at: IndexPath(item: 0, section: 0), animated: true)
    }
    
    private func deselectAllSpecificCategories() {
        for indexPath in viewModel.selectedSpecificCategories {
            filterCollectionView.deselectItem(at: indexPath, animated: true)
        }
        viewModel.selectedSpecificCategories = []
    }
    
    // MARK: - Products Collection View methods
    
    private func productItemDidSelectedAt(indexPath: IndexPath) {
        
        productsCollectionView.deselectItem(at: indexPath, animated: true)
        
        guard let selectedItem = viewModel.productsDataSource.itemIdentifier(for: indexPath) else { return }
        
        // Go to the next screen with selected product
        
        let productVM = ProductVM(product: selectedItem)
        let productVC = ProductVC.instantiate(with: productVM)
        productVC.title = selectedItem.title
        productVM.delegate = productVC
        
        navigationController?.pushViewController(productVC, animated: true)
    }
    
}

// MARK: - AbleToReceiveErrors

extension HomeVC: AbleToReceiveErrors {

    func alertButtonDidTapped(error: HEError) {
        switch error {
        case .unableToComplete:
            // We either don't have a network connection here or we have an invalid endpoint URL.
            viewModel.getCategories()
            
        default:
            // do nothing
            // TODO: present no data state
            break
        }
    }
}

// MARK: - HomeVMDelegate

extension HomeVC: HomeVMDelegate {
    
    func scrollProductsCollectionViewTo(position: IndexPath) {
        productsCollectionView.scrollToItem(at: position,
                                            at: .top,
                                            animated: true)
    }
    
    func selectAllCategoryAnd(fetchData: Bool) {
        selectAllCategory(fetchData: fetchData)
    }
    
    func presentAlert(with error: HEError) {
        
        dismissLoadingView()
        
        DispatchQueue.main.async {
            guard let navigationController = self.navigationController as? HENavigationViewController else {
                return
            }
        
            let title = self.localizedString(for: .somethingWentWrong)
            let message = self.localizedErrorDescription(for: error)
            let buttonTitle = self.localizedString(for: .ok)
            
            navigationController.presentHEAlertOnMainThread(title: title,
                                                            message: message,
                                                            buttonTitle: buttonTitle,
                                                            error: error,
                                                            sender: self)
        }
    }
    
    func startActivityView() {
        showLoadingView()
    }
    
    func stopActivityView() {
        dismissLoadingView()
    }
    
    func selectAllFilter() {
        selectAllCategory()
    }
    
}
