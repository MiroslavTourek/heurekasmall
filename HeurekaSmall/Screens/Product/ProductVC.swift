//
//  ProductVC.swift
//  HeurekaSmall
//
//  Created by Miroslav Tourek on 12/04/2021.
//

import UIKit

class ProductVC: UIViewController, ViewModelable, Localizable {
    
    // MARK: - Properties
    
    var viewModel: ProductVM!
    
    // MARK: - Private properties
    
    private let productImageView = HEImageView(image: UIImage(named: ImageName.placeholder.rawValue)!, cornerRadius: 20)
    private var offersCollectionView: UICollectionView!
    
    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        configureViewController()
        configureNavBar()
        configureStatusBar()
        configureImageView()
        configureCollectionView()
        
        layoutUI()
        
        viewModel.getOffers()
        viewModel.configureOffersDataSource(for: offersCollectionView)
    }
    
    // MARK: - Private methods
    
    private func configureViewController() {
        view.backgroundColor = .systemBackground
    }
    
    private func configureNavBar() {
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    private func configureStatusBar() {
        UIHelper.changeStatusBar(backgroundColor: UIColor(named: HEColors.HEBlue.rawValue)!, at: self)
    }
    
    private func configureImageView() {}
    
    private func configureCollectionView() {
        let collectionViewLayout = viewModel.getOffersCollectionViewLayout()
        offersCollectionView = UICollectionView(frame: .zero,
                                                collectionViewLayout: collectionViewLayout)
        offersCollectionView.delegate = self
        offersCollectionView.backgroundColor = .systemBackground
        offersCollectionView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    private func presentActionSheet(for selectedItem: Offer, at position: IndexPath) {
        
        var isDescriptionAvailable = false
        
        if let offerDescription = selectedItem.description {
            isDescriptionAvailable = !offerDescription.isEmpty
        }
        
        offersCollectionView.scrollToItem(at: position,
                                               at: .top,
                                               animated: true)
        
        let ac = UIAlertController(title: localizedString(for: .whatDoYouWantToDo),
                                   message: nil,
                                   preferredStyle: .actionSheet)
        
        if isDescriptionAvailable {
            ac.addAction(UIAlertAction(title: localizedString(for: .seeOfferDetail),
                                       style: .default,
                                       handler: { _ in
                                        guard let nav = self.navigationController as? HENavigationViewController else { return }
                                        
                                        let title = selectedItem.title ?? ""
                                        let message = selectedItem.description!
                                        let buttonTitle = self.localizedString(for: .ok)
                                        
                                        nav.presentHEAlertOnMainThread(title: title,
                                                                       message: message,
                                                                       buttonTitle: buttonTitle)
                                        }))
        }
        
        ac.addAction(UIAlertAction(title: localizedString(for: .viewWebsite),
                                   style: .default,
                                   handler: { _ in
                                    self.presentSafariVC(with: selectedItem.url)
                                    }))
        
        ac.addAction(UIAlertAction(title: localizedString(for: .cancel),
                                   style: .cancel,
                                   handler: { _ in
                                    self.offersCollectionView.deselectItem(at: position,
                                                                           animated: true)
                                   }))
        
        present(ac, animated: true)
    }
    
    private func layoutUI() {
        
        let leftRightPadding: CGFloat = view.frame.width * 0.03
        let imagePadding: CGFloat = 20
        
        let subviews = [ productImageView, offersCollectionView! ]
        
        subviews.forEach { view.addSubview($0) }
        
        NSLayoutConstraint.activate([
            productImageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: imagePadding),
            productImageView.heightAnchor.constraint(equalToConstant: view.frame.height / 4),
            productImageView.widthAnchor.constraint(equalTo: productImageView.heightAnchor),
            productImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            offersCollectionView.topAnchor.constraint(equalTo: productImageView.bottomAnchor, constant: imagePadding),
            offersCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leftRightPadding),
            offersCollectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -leftRightPadding),
            offersCollectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
}

// MARK: - UICollectionViewDelegate

extension ProductVC: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard let selectedItem = viewModel.offersDataSource.itemIdentifier(for: indexPath) else {
            return
        }
        
        presentActionSheet(for: selectedItem, at: indexPath)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        let onScreenHeight = scrollView.frame.size.height
        
        // Download next page
        if offsetY > contentHeight - onScreenHeight {
            
            viewModel.downloadNextOffers()
        }
    }
    
}

// MARK: - AbleToReceiveErrors

extension ProductVC: AbleToReceiveErrors {
    
    func alertButtonDidTapped(error: HEError) {

        switch error {
        case .unableToComplete:
            // We either don't have a network connection here or we have an invalid endpoint URL.
            viewModel.getOffers()

        default:
            // do nothing
            // TODO: present no data state
            break
        }
    }
    
}

// MARK: - ProductVMDelegate

extension ProductVC: ProductVMDelegate {
    
    func presentAlert(with error: HEError) {
        
        dismissLoadingView()
        
        DispatchQueue.main.async {
            guard let navigationController = self.navigationController as? HENavigationViewController else {
                return
            }
            
            let title = self.localizedString(for: .somethingWentWrong)
            let message = self.localizedErrorDescription(for: error)
            let buttonTitle = self.localizedString(for: .ok)
            
            navigationController.presentHEAlertOnMainThread(title: title,
                                                            message: message,
                                                            buttonTitle: buttonTitle,
                                                            error: error,
                                                            sender: self)
        }
    }
    
    func downloadProductImage(from stringUrl: String, for product: Product) {
        
        DispatchQueue.main.async {
            self.productImageView.downloadImage(from: stringUrl, for: product)
        }
        
    }
    
    func startActivityView() {
        showLoadingView()
    }
    
    func stopActivityView() {
        dismissLoadingView()
    }
    
}
