//
//  ProductVM.swift
//  HeurekaSmall
//
//  Created by Miroslav Tourek on 12/04/2021.
//

import UIKit

// MARK: - Delegate protocol

protocol ProductVMDelegate: class {
    func presentAlert(with error: HEError)
    func downloadProductImage(from stringUrl: String, for product: Product)
    func startActivityView()
    func stopActivityView()
}

// MARK: - Class

class ProductVM {
    
    // MARK: - Properties
    
    weak var delegate: ProductVMDelegate?
    
    var product: Product!
    var offersDataSource: UICollectionViewDiffableDataSource<Section, Offer>!
    
    // MARK: - Private properties
    
    private var offset: Int = 0
    private let limit: Int = 100
    private var offers = [Offer]()
    
    // MARK: - init
    
    init(product: Product) {
        self.product = product
    }
    
    // MARK: - Methods
    
    func getOffersCollectionViewLayout() -> UICollectionViewLayout {
        return UIHelper.createOffersListFlowLayout(interItemSpacing: 15)
    }
    
    func getOffers() {
        
        delegate?.startActivityView()
        
        NetworkManager.shared.getOffers(productId: product.productId, offset: offset, limit: limit) { [weak self] result in
            
            guard let self = self else { return }
            
            self.delegate?.stopActivityView()
            
            switch result {
            
            case .success(let offers):
                self.offers += offers
                self.updateDataSource(with: self.offers)
                self.getProductImageUrl(from: self.offers)
                
            case .failure(let error):
                self.delegate?.presentAlert(with: error)
            }
            
        }
    }
    
    func downloadNextOffers() {
        
        getOffersCount { [weak self] counter in
            guard let self = self,
                  let allOffersCount = counter?.count,
                  allOffersCount > self.offers.count else { return }
            
            self.offset += self.limit
            self.getOffers()
        }
    }
    
    func configureOffersDataSource(for collectionView: UICollectionView) {
        let cellRegistration = UICollectionView.CellRegistration<OfferCollectionCell, Offer> { (cell, indexPath, offer) in
            cell.setup(offerId: offer.offerId, title: offer.title, price: offer.price)
        }
        
        offersDataSource = UICollectionViewDiffableDataSource<Section, Offer>(collectionView: collectionView, cellProvider: { collectionView, indexPath, offer in
            collectionView.dequeueConfiguredReusableCell(using: cellRegistration,
                                                         for: indexPath,
                                                         item: offer)
        })
    }
    
    // MARK: - Private methods
    
    private func getOffersCount(completion: @escaping (Counter?) -> Void){
        
        delegate?.startActivityView()
        
        NetworkManager.shared.getCountOfOffersFor(productId: product.productId) { [weak self] result in
            
            guard let self = self else { return }
            
            self.delegate?.stopActivityView()
            
            switch result {
            
            case .success(let counter):
                completion(counter)
                
            case .failure(let error):
                self.delegate?.presentAlert(with: error)
                completion(nil)
            }
            
        }
    }
    
    private func updateDataSource(with offers: [Offer]) {
        var snapshot = NSDiffableDataSourceSnapshot<Section, Offer>()
        snapshot.appendSections([.main])
        snapshot.appendItems(offers)
        
        DispatchQueue.main.async {
            self.offersDataSource.apply(snapshot, animatingDifferences: true)
        }
    }
    
    private func getProductImageUrl(from offers: [Offer]) {
        
        DispatchQueue.main.async {
            for offer in offers {
                guard let imageUrl = offer.imgUrl,
                      NetworkManager.shared.validate(stringUrl: imageUrl) else { continue }
                
                self.delegate?.downloadProductImage(from: imageUrl, for: self.product)
                break
            }
        }
        
    }
}
