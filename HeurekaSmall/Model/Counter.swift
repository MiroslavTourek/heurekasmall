//
//  Counter.swift
//  HeurekaSmall
//
//  Created by Miroslav Tourek on 07/04/2021.
//

struct Counter: Codable {
    var count: Int?
}
