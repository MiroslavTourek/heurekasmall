//
//  Category.swift
//  HeurekaSmall
//
//  Created by Miroslav Tourek on 07/04/2021.
//

struct Category: Codable, Hashable {
    let categoryId: Int
    let title: String?
}
