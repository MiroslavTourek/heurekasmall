//
//  Product.swift
//  HeurekaSmall
//
//  Created by Miroslav Tourek on 07/04/2021.
//

struct Product: Codable, Hashable {
    let categoryId: Int
    let productId: Int
    let title: String?
}
