//
//  ProductSectionItem.swift
//  HeurekaSmall
//
//  Created by Miroslav Tourek on 11/04/2021.
//

struct ProductSectionItem {
    let sectionId: Int
    var title: String?
    var products: [Product]
}

