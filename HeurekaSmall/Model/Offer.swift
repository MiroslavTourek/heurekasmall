//
//  Offer.swift
//  HeurekaSmall
//
//  Created by Miroslav Tourek on 07/04/2021.
//

struct Offer: Codable, Hashable {
    let description: String?
    let imgUrl: String?
    let offerId: Int
    let price: Float?
    let productId: Int
    let title: String?
    let url: String?
}
