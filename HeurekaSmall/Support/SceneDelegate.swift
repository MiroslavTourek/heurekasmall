//
//  SceneDelegate.swift
//  HeurekaSmall
//
//  Created by Miroslav Tourek on 07/04/2021.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let scene = (scene as? UIWindowScene) else { return }
        
        window = UIWindow(frame: scene.coordinateSpace.bounds)
        window?.windowScene = scene
        window?.rootViewController = createRootViewController()
        window?.makeKeyAndVisible()
    }
    
    // MARK: - Private methods
    
    // TODO: Move to Coordinator
    
    private func createRootViewController() -> UIViewController {
        let homeVM = HomeVM()
        let homeVC = HomeVC.instantiate(with: homeVM)
        homeVM.delegate = homeVC
        
        return HENavigationViewController(rootViewController: homeVC)
    }
}

