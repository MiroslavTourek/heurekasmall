//
//  AbleToReceiveErrors.swift
//  HeurekaSmall
//
//  Created by Miroslav Tourek on 13/04/2021.
//

protocol AbleToReceiveErrors: class {
    
    func alertButtonDidTapped(error: HEError)
    
}
