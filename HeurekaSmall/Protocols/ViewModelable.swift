//
//  ViewModelable.swift
//  HeurekaSmall
//
//  Created by Miroslav Tourek on 09/04/2021.
//

import UIKit

protocol ViewModelable: class {
    associatedtype ViewModelType
    var viewModel: ViewModelType! { get set }
}

// Default implementation for UIViewControllers
extension ViewModelable where Self: UIViewController {
    
    /// Instantiates and returns a UIViewController instance with a related ViewModel.
    /// - Parameter viewModel: Related view model
    /// - Returns: UIViewController instance
    static func instantiate<ViewModelType>(with viewModel: ViewModelType) -> Self where ViewModelType == Self.ViewModelType {
        
        let vc = Self.init()
        vc.viewModel = viewModel
        return vc
    }
}
