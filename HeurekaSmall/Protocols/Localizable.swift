//
//  Localizable.swift
//  HeurekaSmall
//
//  Created by Miroslav Tourek on 12/04/2021.
//

import UIKit

protocol Localizable: class { }

// Default implementation
extension Localizable {
    
    func localizedString(for key: LocalizationKey) -> String {
        NSLocalizedString(key.rawValue, comment: "")
    }
    
    func localizedErrorDescription(for error: HEError) -> String {
        NSLocalizedString(error.rawValue, comment: "")
    }
}
