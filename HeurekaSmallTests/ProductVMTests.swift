//
//  ProductVMTests.swift
//  HeurekaSmallTests
//
//  Created by Miroslav Tourek on 14/04/2021.
//

import XCTest

class ProductVMTests: XCTestCase {
    
    /// Test home view model.
    var productVM: ProductVM!

    override func setUpWithError() throws {
        productVM = ProductVM(product: Product(categoryId: 1, productId: 10, title: "Product title"))
    }

    override func tearDownWithError() throws {
        productVM = nil
    }

    func testOfObjectInitialization() {
        XCTAssertNotNil(productVM.product, "Product property can not be nil.")
        XCTAssert(productVM.product.categoryId == 1)
        XCTAssert(productVM.product.productId == 10)
        XCTAssert(productVM.product.title == "Product title")
    }
    
    
}
