//
//  HomeVMTests.swift
//  HeurekaSmallTests
//
//  Created by Miroslav Tourek on 14/04/2021.
//

import XCTest

class HomeVMTests: XCTestCase {
    
    /// Test home view model.
    var homeVM: HomeVM!

    override func setUpWithError() throws {
        homeVM = HomeVM()
    }

    override func tearDownWithError() throws {
        homeVM = nil
    }
    
    func testSectionItemsCreationFromEmptyGroupedProducts() {
        let groupedProducts: [Int : [Product]] = [:]
        
        let sectionItems = homeVM.createSectionItems(from: groupedProducts)
        
        XCTAssert(sectionItems.count == 0, "Section items must be empty.")
    }
    
    func testSectionItemsCreationFromGroupedProducts() {
        let product = Product(categoryId: 1, productId: 10, title: "Product title")
        let groupedProducts = [20: [product]]
        homeVM.categories = [Category(categoryId: 20, title: "Category title")]
        
        let sectionItems = homeVM.createSectionItems(from: groupedProducts)
        print()
        
        XCTAssert(sectionItems.count == 1, "Section items count must be equal to 1.")
        XCTAssert(sectionItems.first!.sectionId == 20, "The ID must be equal to 20.")
        XCTAssert(sectionItems.first!.products.count == 1, "The number of products must be equal to 1.")
        XCTAssert(sectionItems.first!.title == "Category title", "The title must be 'Category title'.")
        
        homeVM.categories = []
    }
    
    func testOfUpdateSectionItemsWithGroupedProductsMethod() {
        let selectedSections = [
            ProductSectionItem(sectionId: 1, title: "Section title", products: [])
        ]
        homeVM.selectedSections = selectedSections
        
        let groupedProducts = [
            1: [
                Product(categoryId: 1, productId: 1, title: "Product with id 1"),
                Product(categoryId: 1, productId: 2, title: "Product with id 2"),
            ],
            2: [
                Product(categoryId: 2, productId: 11, title: "Product with id 11")
            ]
        ]
        
        let updatedSelectedSections = homeVM.updateSectionItems(with: groupedProducts)
        
        XCTAssertFalse(updatedSelectedSections.isEmpty)
        XCTAssert(updatedSelectedSections.first!.products.count == 2)
        XCTAssertFalse(updatedSelectedSections.first!.products.contains(where: { product in
            product.categoryId == 2
        }))
        XCTAssertFalse(updatedSelectedSections.first!.products.contains(where: { product in
            product.productId == 1 && product.title == "Product with id 2"
        }))
        
        homeVM.selectedSections = []
    }
    
    func testOfRemovingFromSelectedCategories() {
        homeVM.selectedSpecificCategories = []
        
        for number in 0..<10 {
            let indexPath = IndexPath(item: number, section: 0)
            homeVM.selectedSpecificCategories.append(indexPath)
        }
        
        var indexPath = IndexPath(item: 10, section: 0)
        homeVM.removeCategoryFromSelected(indexPath: indexPath)
        
        XCTAssert(homeVM.selectedSpecificCategories.count == 10)
        
        indexPath = IndexPath(item: 9, section: 1)
        homeVM.removeCategoryFromSelected(indexPath: indexPath)
        
        XCTAssert(homeVM.selectedSpecificCategories.count == 10)
        
        indexPath = IndexPath(item: 9, section: 0)
        homeVM.removeCategoryFromSelected(indexPath: indexPath)
        
        XCTAssert(homeVM.selectedSpecificCategories.count == 9)
        
        indexPath = IndexPath(item: 9, section: 0)
        homeVM.removeCategoryFromSelected(indexPath: indexPath)
        
        XCTAssert(homeVM.selectedSpecificCategories.count == 9, "It is not possible to delete one thing more than once.")
        
        indexPath = IndexPath(item: 4, section: 0)
        homeVM.removeCategoryFromSelected(indexPath: indexPath)
        
        XCTAssert(homeVM.selectedSpecificCategories.count == 8)
        
        indexPath = IndexPath(item: 0, section: 0)
        homeVM.removeCategoryFromSelected(indexPath: indexPath)
        
        XCTAssert(homeVM.selectedSpecificCategories.count == 7)
        
        homeVM.selectedSpecificCategories = []
    }
    
    func testOfAddingToSelectedCategories() {
        homeVM.selectedSpecificCategories = []
        
        for number in 0..<10 {
            let indexPath = IndexPath(item: number, section: 0)
            homeVM.addCategoryToSelected(indexPath: indexPath)
            
            guard number.isMultiple(of: 2) else { continue }
            homeVM.addCategoryToSelected(indexPath: indexPath)
        }
        
        XCTAssert(homeVM.selectedSpecificCategories.count == 10)
        let count = (homeVM.selectedSpecificCategories.filter { $0.item == 2 }).count
        XCTAssertFalse( count == 2, "It can not add one thing more than once.")
        XCTAssert( count == 1, "It can not add one thing more than once.")
        
        homeVM.selectedSpecificCategories = []
    }

}
